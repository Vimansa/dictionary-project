function checkTone(){
    var x = $("#search-bar").val();
    console.log(x);
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://text-analytics-by-symanto.p.rapidapi.com/communication",
        "method": "POST",
        "headers": {
            "x-rapidapi-host": "text-analytics-by-symanto.p.rapidapi.com",
            "x-rapidapi-key": "600044bc98msh9c9ae16cbec59eap1fdce8jsnf65c4ada951f",
            "content-type": "application/json",
            "accept": "application/json"
        },
        "processData": false,
        "data": "[ {  \"id\": \"1\",  \"language\": \"en\",  \"text\": \""+ x + "\" }]"
    }
    
    $.ajax(settings).done(function (response) {
        console.log(response);
        $("#tonepara").text(response[0].predictions[0].prediction);
        $("#toneProbability").text(response[0].predictions[0].probability);
        checkSenti();
    });
}
function checkSenti(){
    var x = $("#search-bar").val();
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://text-analytics-by-symanto.p.rapidapi.com/sentiment",
        "method": "POST",
        "headers": {
            "x-rapidapi-host": "text-analytics-by-symanto.p.rapidapi.com",
            "x-rapidapi-key": "600044bc98msh9c9ae16cbec59eap1fdce8jsnf65c4ada951f",
            "content-type": "application/json",
            "accept": "application/json"
        },
        "processData": false,
        "data": "[ {  \"id\": \"1\",  \"language\": \"en\",  \"text\": \""+ x + "\" }]"
    }
    
    $.ajax(settings).done(function (response) {
        console.log(response);
        $("#emopara").text(response[0].predictions[0].prediction);
        $("#emoProbability").text(response[0].predictions[0].probability);
    });
}