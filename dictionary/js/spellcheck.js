var para = "";
var home = "";
function checkSpell() {
    clearAll();
    setPara();
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://montanaflynn-spellcheck.p.rapidapi.com/check/?text=" + para,
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "montanaflynn-spellcheck.p.rapidapi.com",
            "x-rapidapi-key": "600044bc98msh9c9ae16cbec59eap1fdce8jsnf65c4ada951f"
        }
    }

    $.ajax(settings).done(function (response) {
        $("#suggestions").append(response.suggestion);
        setCorrections(response);
    });
}

function setPara() {
    var str = $('#spellcheck-input').val();
    para = str.toString().split(" ").join("%20");
    console.log(para);
}

function setCorrections(data) {
    console.log(data);
    var temp = Object.keys(data.corrections);
    console.log(temp);
    var html = "<ol>";
    for (var x in data.corrections) {
        html += "<li>" + x + "</li>";
        html +="<ul>";
        for (var j = 0; j < data.corrections[x].length; j++) {
            html +="<li>" + data.corrections[x][j] + "</li>";
        }
        html +="</ul>";
    }
    html +="</ol>";
    $("#corrections").html(html);
}
function clearAll(){
    $("#corrections").text("");
    $("#suggestions").text("");
}
function setText(){
    $('#spellcheck-input').val(localStorage.getItem('input'));
}

