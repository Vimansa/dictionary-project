var url1;
var word;
var url2;
var key = "l20bvreebgoiu97txa485zr0otkp19suifungpga3fmwu27pi";

//common funcs
$(function() {
    $("form").submit(function() { return false; });
});
$('#search-bar').keypress(function(e){
    console.log("key pressed");
    if(e.which == 13){//Enter key pressed
        console.log("enter pressed");
        setAll();
    }
});



function setAll() {
    clearAll();
    if (getWord()) {
        getDef();
        getRelWords();
        getAudio();
    }
}

function getWord() {
    var tempWord = $("#search-bar").val();
    if (tempWord.indexOf(' ') === -1) {
        word = tempWord.toLowerCase();
        console.log(word);
        return true;
    } else {
        alert("please enter a single word");
        return false;
    }

}

function clearAll() {
    $('#def-para').text("");
    $('#syn-para').text("");
    $('#ant-para').text("");
    $('#hyp-para').text("");
    $('#audio').text("");
}
function getAudio(){
    url1 = "https://api.wordnik.com/v4/word.json/";
    url2 = "/audio?useCanonical=false&limit=50&api_key=";
    var url = url1 + word + url2 + key;
    $.getJSON(url, function (data) {
        console.log(data);

        var x = data[0].fileUrl;
        $("#audio").append("<audio controls style=\"display: inline-block\" src=\"" + x + "\">");

    });
}

//definition
function getDef() {
    console.log("ran getDef");
    url1 = "https://api.wordnik.com/v4/word.json/";
    url2 = "/definitions?limit=200&includeRelated=false&sourceDictionaries=all&useCanonical=false&includeTags=false&api_key=";
    var url = url1 + word + url2 + key;
    $.ajax(url,
        {
            dataType: 'json', // type of response data
            //timeout: 2000,     // timeout milliseconds
            success: function (data, status, xhr) {   // success callback function
                setDef(data);
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback 
                if (jqXhr.status == '404') {
                    alert("No Definition Found");
                }
            }
        });
}

function setDef(data) {

    if (data !== null) {
        $('#def-para').append("<ul>");
        for (var i = 0; i < data.length; i++) {
            if (data[i].text !== undefined) {
                $('#def-para').append("<li>" + data[i].text + "</li><br>");
            }
        }
        $('#def-para').append("</ul>");
    }
}

//related words

function getRelWords() {
    console.log("ran getsyn");
    url1 = "https://api.wordnik.com/v4/word.json/";
    url2 = "/relatedWords?useCanonical=false&limitPerRelationshipType=10&api_key=";
    var url = url1 + word + url2 + key;
    $.getJSON(url, function (data) {
        console.log("got rels");
        setRelWords(data);
    });
}
function setRelWords(data) {

    if (data !== null) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].relationshipType === "synonym") {
                $('#syn-para').append("<ul>");
                for (var j = 0; j < data[i].words.length; j++) {
                    if (data[i].words[j] !== undefined) {
                        $('#syn-para').append("<li>" + data[i].words[j] + "</li><br>");
                    }
                }
                $('#syn-para').append("</ul>");
            }
            if (data[i].relationshipType === "antonym") {
                $('#ant-para').append("<ul>");
                for (var j = 0; j < data[i].words.length; j++) {
                    if (data[i].words[j] !== undefined) {
                        $('#ant-para').append("<li>" + data[i].words[j] + "</li><br>");
                    }
                }
                $('#ant-para').append("</ul>");
            }
            if (data[i].relationshipType === "hypernym") {
                $('#hyp-para').append("<ul>");
                for (var j = 0; j < data[i].words.length; j++) {
                    if (data[i].words[j] !== undefined) {
                        $('#hyp-para').append("<li>" + data[i].words[j] + "</li><br>");
                    }
                }
                $('#hyp-para').append("</ul>");
            }  
        }
    }
}
